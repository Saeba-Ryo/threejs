'use strict';

Physijs.scripts.worker = '/js/vendor/physijs_worker.js';
Physijs.scripts.ammo = '/js/vendor/ammo.js';

// USED VARS
var initScene, render, renderer, scene, camera, ball, platform;

// SCENE SETUP
initScene = function() {
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.getElementById( 'viewport' ).appendChild( renderer.domElement );
    
    scene = new Physijs.Scene();
    scene.setGravity(new THREE.Vector3( 0, -200, 0 ));
    
    camera = new THREE.PerspectiveCamera(
        35,
        window.innerWidth / window.innerHeight,
        1,
        2000
    );
    camera.position.set( 0, 0, 200 );
    camera.lookAt( scene.position );
    scene.add( camera );
    requestAnimationFrame( render );
};

// PREPARE PHYSIC ENGINE
render = function() {
    scene.simulate(); // run physics
    renderer.render( scene, camera); // render the scene
    requestAnimationFrame( render );
};

window.onload = initScene();

// ELEMENTS POSITIONNING
ball = createBall(0);
ball.position.set(0, 50, 0); // x y z
scene.add( ball );

// platform
platform = createPlatform(0);
scene.add( platform );


/********************
 OBJECTS CREATION AND STATE CHECKING
*********************/

// CREATE A BALL (0 for no gravity)
function createBall(mass){
    return (
        new Physijs.SphereMesh(
            new THREE.SphereGeometry( 3, 32, 32 ),
            new THREE.MeshBasicMaterial({ color: 0xffffff }),
            mass
        )
    )
}

// CREATE PLATFORM (0 for no gravity)
function createPlatform(mass){
    return (
        new Physijs.BoxMesh(
        new THREE.BoxGeometry( 60, 4, 0 ),
        new THREE.MeshBasicMaterial({ color: 0xffffff }),
        mass
        )
    )
}

// PREPARE BALL
let ballPos
function ballPosition() {
    ballPos = requestAnimationFrame( ballPosition );
    isBallOut(); 
}
ballPosition()

// RECTANGLE ANIMATION 
let rotatingPlatform;
let angle = 0;
    function rotate() {
        rotatingPlatform = requestAnimationFrame( rotate );
        platform.rotation.set(0, 0, angle += .05);
        platform.__dirtyRotation = true;
        renderer.render( scene, camera );
}
rotate();

/********************
 HANDLING CLICK ON WINDOW
*********************/

window.addEventListener("click", () => {STOProtation(); dropBall()})

function dropBall(){
    console.log("ball dropped !");
    scene.remove(ball);
    ball = createBall(1);
    ball.position.set(0, 50, 0);
    scene.add( ball );
}

function STOProtation(){
    cancelAnimationFrame( rotatingPlatform )
}

function isBallOut(){
    // get scene size
    let limits = screenLimits();
    let isInIntervalX = ball.position.x > limits.x1 && ball.position.x < limits.x2 ? true : false;
    let isInIntervalY = ball.position.y > limits.y1 && ball.position.x < limits.y2 ? true : false;
    
    if(!isInIntervalX || !isInIntervalY){
        RESET()
    }
}

function RESET(){
    scene.remove(ball);
    ball = createBall(0);
    ball.position.set(0, 50, 0);
    scene.add( ball );
    rotate();
}

/******************************
 WINDOW CHANGES HANDLING
************/

// RESIZE WINDOW PROPERLY
window.addEventListener( 'resize', onWindowResize, false );

function onWindowResize(){
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

// GET SCENE LIMITATIONS VALUES
function screenLimits(){
    // size of the scene
    let vFOV = THREE.Math.degToRad( camera.fov ); // convert vertical fov to radians
    let height = 2 * Math.tan( vFOV / 2 ) * 200; // visible height
    let width = height * camera.aspect;

    // size of the ball must be taken into account in every directions
    let ballSize = ball._physijs.radius / 2;
    let limits = {
        x1 : ((width / 2) * - 1) - ballSize,
        x2 : (width / 2) + ballSize,
        y1 : ((height / 2) * -1) - ballSize,
        y2 : (height / 2) + ballSize
    }
    return limits
}