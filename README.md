TEST THREE JS
=================

1. Le temps de réalisation
2. Les décisions techniques
3. Difficultés rencontrées
4. Améliorations
5. Aller plus loin
6. Ma conculsion
7. Commentaires.

1 Temps de réalisation
----------------------

__Création des éléments dans la scène : 2h__

__Ajout du moteur physique et des interactions : 2h__

__Vérification de la sortie de la balle de l'écran : 1h__

__Documentation, tests et choix de librairie : 11h__


2 Les décisions techniques
--------------------------

Après avoir fait quelques tests en essayant de gérer la collision et regardant des documentations sur le sujet. J'ai vu qu'un moteur physique s'imposerait...

J'ai décidé de prendre _Phyisi.js_ comme moteur physique, car la documentation
était claire et que celle-ci est particulièrement destinée à fonctionner avec _Three.js_

3 Difficultés rencontrées
-------------------------

N'ayant jamais fait de 3D j'ai dû me documenter pour apprendre les termes techniques utilisés notamment pour l'utilisation du moteur physique.

Je n'ai pas trouvé de moteur tout préparé dans _Three.js_. J'ai donc d'abord fait quelques essais avec une première librairie (matter.js) dont j'avais entendu parler. Mais son utilisation avec __Three.js__ n'était pas simple.

4 Améliorations
---------------

Tester plusieurs librairies afin d'en connaître une me convenant et ainsi me permettre d'être plus efficace.

5 Aller plus loin
-----------------

On pourrait rajouter un compteur de balles lancées, un input avec choix de l'angle de la plateforme, un bouton pour faire tourner la plateforme dans l'autre sens, modifier la gravité, ajouter du rebond à la balle, faire tomber la balle dans des sceaux rapportant des points...

6 Conclusion
------------

Faire un moteur physique me parait extrêmement complexe.
Javacript à décidemment énormément de possibilités et beaucoup de librairies et il est parfois difficile de s'y retrouver parmi tout cela.
On se surprend de s'amuser à réessayer de faire tomber la balle dans des axes différents.

7 Commentaires
--------------

J'ai trouvé ce test très intéressant et enrichissant, et cela m'a donné envie d'en savoir plus sur les librairies 3D ainsi que les moteurs physiques.

